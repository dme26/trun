/** trun/detail/common.hpp ---
 *
 * Copyright (C) 2015-2023 Lluís Vilanova
 *
 * Author: Lluís Vilanova <vilanova@imperial.ac.uk>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */

#ifndef TRUN__DETAIL__COMMON_HPP
#define TRUN__DETAIL__COMMON_HPP 1

#include <err.h>


#define __unused(arg) _unused_ ## arg __attribute__((unused))

namespace trun {
    namespace detail {

#pragma GCC push_options
#pragma GCC diagnostic ignored "-Wformat-security"
        template<trun::message level, trun::message req, class... Args>
        static inline
        void message(const std::string & msg, Args&&...args)
        {
            if (req >= level) {
                std::string s = "[trun] " + msg;
                warnx(s.c_str(), std::forward<Args>(args)...);
            }
        }
#pragma GCC pop_options

    }
}

#endif // TRUN__DETAIL__COMMON_HPP
