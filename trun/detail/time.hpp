/** trun/detail/time.hpp ---
 *
 * Copyright (C) 2015-2023 Lluís Vilanova
 *
 * Author: Lluís Vilanova <vilanova@imperial.ac.uk>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */

#ifndef TRUN__DETAIL__TIME_HPP
#define TRUN__DETAIL__TIME_HPP 1

#include <trun/detail/core.hpp>


namespace trun {
    namespace time {

        namespace detail {

            template<class Clock>
            static inline
            void check(const Clock &clock)
            {
            }

        }

        template<class C, trun::message msg>
        static inline
        parameters<C> calibrate(const parameters<C> & params)
        {
            trun::detail::parameters::check(params);
            trun::detail::message<trun::message::INFO, msg>("Calibrating clock overheads...");
            auto time = []() {
                auto t1 = C::now();
                (void)t1;
                auto t2 = C::now();
                (void)t2;
            };
            auto res = trun::detail::core::run<true, msg, C>(params, time);
            if (!res.converged) {
                errx(1, "[trun] clock calibration did not converge");
            }

            parameters<C> res_params;
            res_params.clock_time = res.mean;

            return res_params;
        }

        template<class C, trun::message msg, class F>
        static inline
        parameters<C> calibrate(const parameters<C> & params, F&& func, trun::mod_clock_type &mod_clock)
        {
            // initialize parameters
            trun::detail::parameters::check(params);

            trun::detail::message<trun::message::INFO, msg>("Calibrating clock overheads...");
            auto res = trun::detail::core::run<true, msg, C>(params, func, mod_clock);
            if (!res.converged) {
                errx(1, "[trun] clock calibration did not converge");
            }

            parameters<C> res_params;
            res_params.clock_time = res.mean;

            return res_params;
        }

        template<class C, trun::message msg>
        static inline
        parameters<C> calibrate()
        {
            parameters<C> clock_params = trun::parameters<C>::get_clock_params();
            parameters<C> res_params = calibrate<C, msg>(clock_params);
            return res_params;
        }

        template<class C, trun::message msg, class F>
        static inline
        parameters<C> calibrate(F&& func, trun::mod_clock_type &mod_clock)
        {
            parameters<C> clock_params = trun::parameters<C>::get_clock_params();
            parameters<C> res_params = calibrate<C, msg>(clock_params, std::forward<F>(func), mod_clock);
            return res_params;
        }

        template<class Ratio, class Clock>
        static inline
        std::string units(const Clock &clock)
        {
            std::string units = "?";
            if (std::ratio_equal<std::femto, Ratio>::value) {
                units = "fs";
            } else if (std::ratio_equal<std::pico, Ratio>::value) {
                units = "ps";
            } else if (std::ratio_equal<std::nano, Ratio>::value) {
                units = "ns";
            } else if (std::ratio_equal<std::micro, Ratio>::value) {
                units = "us";
            } else if (std::ratio_equal<std::milli, Ratio>::value) {
                units = "ms";
            } else if (std::ratio_equal<std::ratio<1>, Ratio>::value) {
                units = "sec";
            } else if (std::ratio_equal<std::ratio<60>, Ratio>::value) {
                units = "min";
            } else if (std::ratio_equal<std::ratio<60*60>, Ratio>::value) {
                units = "hours";
            } else if (std::ratio_equal<std::ratio<60*60*24>, Ratio>::value) {
                units = "days";
            }
            return units;
        }

    }
}

#include <trun/detail/time-cycles.hpp>

#endif // TRUN__DETAIL__TIME_HPP
